module gitlab.com/sacules/chartsy

go 1.20

require (
	github.com/Sacules/lrserver v0.0.0-20230722195831-1d5bea1fc136
	github.com/a-h/templ v0.2.543
	github.com/alexedwards/scs/sqlite3store v0.0.0-20231113091146-cef4b05350c8
	github.com/alexedwards/scs/v2 v2.7.0
	github.com/fsnotify/fsnotify v1.7.0
	github.com/go-chi/chi/v5 v5.0.11
	github.com/go-chi/httprate v0.8.0
	github.com/go-chi/render v1.0.3
	github.com/go-playground/form/v4 v4.2.1
	github.com/jmoiron/sqlx v1.3.5
	github.com/joho/godotenv v1.5.1
	github.com/mattn/go-sqlite3 v1.14.19
	github.com/xhit/go-simple-mail/v2 v2.16.0
	golang.org/x/crypto v0.18.0
	golang.org/x/exp v0.0.0-20240119083558-1b970713d09a
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/go-test/deep v1.1.0 // indirect
	github.com/gorilla/websocket v1.5.1 // indirect
	github.com/jaschaephraim/lrserver v0.0.0-20171129202958-50d19f603f71 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	github.com/toorop/go-dkim v0.0.0-20240103092955-90b7d1423f92 // indirect
	golang.org/x/net v0.20.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
)
