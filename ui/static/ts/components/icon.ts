import { html } from 'lit';
import { customElement } from 'lit/decorators.js';
import { BaseElement } from './base';

@customElement('icon-minus')
export class IconMinus extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
				<g clip-path="url(#a)">
					<path
						stroke="currentColor"
						stroke-linecap="round"
						stroke-linejoin="round"
						d="M6.889 12H17.11m6.389 0c0 6.351-5.149 11.5-11.5 11.5S.5 18.351.5 12 5.649.5 12 .5 23.5 5.649 23.5 12Z"
					/>
				</g>
				<defs>
					<clipPath id="a">
						<path fill="currentColor" d="M0 0h24v24H0z" />
					</clipPath>
				</defs>
			</svg>
		`;
	}
}

@customElement('icon-plus')
export class IconPlus extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
				<g clip-path="url(#a)">
					<path
						stroke="currentColor"
						stroke-linecap="round"
						stroke-linejoin="round"
						d="M6.889 12H17.11M12 6.889V17.11M23.5 12c0 6.351-5.149 11.5-11.5 11.5S.5 18.351.5 12 5.649.5 12 .5 23.5 5.649 23.5 12Z"
					/>
				</g>
				<defs>
					<clipPath id="a">
						<path fill="currentColor" d="M0 0h24v24H0z" />
					</clipPath>
				</defs>
			</svg>
		`;
	}
}

@customElement('icon-settings')
export class IconSettings extends BaseElement {
	override render() {
		return html`
			<svg
				width="23.999996"
				height="24"
				viewBox="0 0 0.71999989 0.72"
				version="1.1"
				id="svg1"
				sodipodi:docname="sliders-svgrepo-com.svg"
				inkscape:export-filename="sliders-svgrepo-com-edited.svg"
				inkscape:export-xdpi="96"
				inkscape:export-ydpi="96"
				xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
				xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
				xmlns="http://www.w3.org/2000/svg"
				xmlns:svg="http://www.w3.org/2000/svg"
			>
				<path
					d="m 0.35999995,0.1108345 c 0,0.052927 -0.0429064,0.0958329 -0.0958329,0.0958329 -0.0529269,0 -0.0958329,-0.0429059 -0.0958329,-0.0958329 m 0.19166569,0 c 0,-0.05292698 -0.0429064,-0.09583288 -0.0958329,-0.09583288 -0.0529269,0 -0.0958328,0.0429059 -0.0958328,0.09583288 m 0.19166569,0 h 0.34499819 m -0.53666394,0 H 0.01500169 M 0.6283319,0.36000002 c 0,0.0529265 -0.0429064,0.0958329 -0.0958328,0.0958329 -0.0529266,0 -0.0958328,-0.0429063 -0.0958328,-0.0958329 m 0.19166568,0 c 0,-0.0529266 -0.0429064,-0.0958329 -0.0958328,-0.0958329 -0.0529266,0 -0.0958328,0.0429062 -0.0958328,0.0958329 m 0.19166568,0 h 0.0766663 m -0.26833198,0 H 0.01500169 m 0.26833199,0.24916552 c 0,0.0529266 -0.0429059,0.0958329 -0.0958329,0.0958329 -0.0529269,0 -0.09583282,-0.0429063 -0.09583282,-0.0958329 m 0.1916657,0 c 0,-0.0529265 -0.0429059,-0.0958329 -0.0958328,-0.0958329 -0.0529269,0 -0.09583283,0.0429063 -0.09583283,0.0958329 m 0.1916657,0 h 0.42166441 m -0.61333021,0 H 0.01500162"
					stroke="currentColor"
					stroke-width="0.072"
					stroke-linecap="round"
					stroke-linejoin="round"
					style="stroke-width:0.0300032;stroke-dasharray:none"
				/>
			</svg>
		`;
	}
}

@customElement('icon-layout')
export class IconLayout extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 25 24">
				<path
					stroke="currentColor"
					stroke-linecap="round"
					stroke-linejoin="round"
					d="M14.875 2.8c0-.804 0-1.207.156-1.514.138-.27.358-.49.629-.629.307-.156.71-.156 1.515-.156h4.024c.805 0 1.208 0 1.515.156.27.138.49.358.629.629.156.307.156.71.156 1.515v4.024c0 .805 0 1.208-.156 1.515-.138.27-.358.49-.629.629-.307.156-.71.156-1.515.156h-4.024c-.806 0-1.208 0-1.515-.156a1.437 1.437 0 0 1-.629-.629c-.156-.307-.156-.71-.156-1.515V2.801ZM.5 2.8c0-.804 0-1.207.157-1.514.138-.27.358-.49.629-.629.307-.156.71-.156 1.515-.156h4.024C7.63.5 8.033.5 8.34.657c.27.138.49.358.629.629.156.307.156.71.156 1.515v4.024c0 .805 0 1.208-.156 1.515-.138.27-.358.49-.629.629-.307.156-.71.156-1.515.156H2.801c-.805 0-1.208 0-1.515-.156a1.438 1.438 0 0 1-.629-.629C.501 8.033.501 7.63.501 6.825V2.801Zm0 14.375c0-.806 0-1.208.157-1.515.138-.27.358-.49.629-.629.307-.156.71-.156 1.515-.156h4.024c.805 0 1.208 0 1.515.156.27.138.49.358.629.629.156.307.156.71.156 1.515v4.024c0 .805 0 1.208-.156 1.515-.138.27-.358.49-.629.629-.307.156-.71.156-1.515.156H2.801c-.805 0-1.208 0-1.515-.156a1.438 1.438 0 0 1-.629-.629c-.156-.307-.156-.71-.156-1.515v-4.024Zm14.375 0c0-.806 0-1.208.156-1.515.138-.27.358-.49.629-.629.307-.156.71-.156 1.515-.156h4.024c.805 0 1.208 0 1.515.156.27.138.49.358.629.629.156.307.156.71.156 1.515v4.024c0 .805 0 1.208-.156 1.515-.138.27-.358.49-.629.629-.307.156-.71.156-1.515.156h-4.024c-.806 0-1.208 0-1.515-.156a1.437 1.437 0 0 1-.629-.629c-.156-.307-.156-.71-.156-1.515v-4.024Z"
				/>
			</svg>
		`;
	}
}

@customElement('icon-image')
export class IconImage extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
				<g clip-path="url(#a)">
					<path
						stroke="currentColor"
						stroke-linecap="round"
						stroke-linejoin="round"
						d="m15.254 17.66-2.398-2.378c-1.157-1.147-1.735-1.72-2.399-1.93a2.876 2.876 0 0 0-1.791.019c-.66.224-1.225.81-2.357 1.982L.564 21.027m14.69-3.367.491-.487c1.159-1.148 1.738-1.723 2.402-1.933a2.874 2.874 0 0 1 1.793.021c.66.225 1.225.813 2.357 1.988l1.203 1.21m-8.246-.799 5.766 5.777M.564 21.027c.044.363.12.649.25.903.275.54.715.98 1.256 1.256.615.314 1.42.314 3.03.314h13.8c.94 0 1.606 0 2.12-.063M.564 21.027C.5 20.513.5 19.845.5 18.9V5.1c0-1.61 0-2.415.314-3.03.275-.54.715-.98 1.256-1.256C2.685.5 3.49.5 5.1.5h13.8c1.61 0 2.415 0 3.03.314.54.275.98.715 1.256 1.256.314.615.314 1.42.314 3.03v13.36m0 0v.44c0 1.61 0 2.416-.314 3.031-.275.54-.715.98-1.256 1.256-.256.13-.544.207-.91.251m-1.833-15.75a2.875 2.875 0 1 1-5.75 0 2.875 2.875 0 0 1 5.75 0Z"
					/>
				</g>
				<defs>
					<clipPath id="a">
						<path fill="#fff" d="M0 0h24v24H0z" />
					</clipPath>
				</defs>
			</svg>
		`;
	}
}

@customElement('icon-text')
export class IconText extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" fill="none" viewBox="0 0 18 22">
				<path
					stroke="currentColor"
					stroke-linecap="round"
					stroke-linejoin="round"
					d="M9 1v20m-3.429 0h6.858M17 4.333V1H1v3.333"
				/>
			</svg>
		`;
	}
}

@customElement('icon-burger')
export class IconBurger extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="18" fill="none" viewBox="0 0 26 18">
				<path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" d="M1 1h24M1 9h24M1 17h24" />
			</svg>
		`;
	}
}

@customElement('icon-download')
export class IconDownload extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 26 26">
				<path
					stroke="currentColor"
					stroke-linecap="round"
					stroke-linejoin="round"
					d="M19.667 20.5h.013m.52-4.5h.8c1.242 0 1.864 0 2.354.228.653.305 1.172.889 1.443 1.624.203.551.203 1.25.203 2.648 0 1.398 0 2.097-.203 2.648-.27.735-.79 1.32-1.443 1.624C22.864 25 22.243 25 21 25H5c-1.243 0-1.864 0-2.354-.228-.653-.305-1.172-.889-1.443-1.624C1 22.597 1 21.898 1 20.5c0-1.398 0-2.097.203-2.648.27-.735.79-1.32 1.443-1.624C3.136 16 3.757 16 5 16h.8m7.2 1.5V1m0 16.5L9 13m4 4.5 4-4.5"
				/>
			</svg>
		`;
	}
}

@customElement('icon-user-plus')
export class IconUserPlus extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 26 26">
				<path
					stroke="currentColor"
					stroke-linecap="round"
					stroke-linejoin="round"
					d="M25 21h-4.5m0 0H16m4.5 0v-4m0 4v4m-9 0H1c0-5.155 4.701-9.333 10.5-9.333 1.043 0 2.05.135 3 .386m3-9.72c0 2.946-2.686 5.334-6 5.334s-6-2.388-6-5.334C5.5 3.388 8.186 1 11.5 1s6 2.388 6 5.333Z"
				/>
			</svg>
		`;
	}
}

@customElement('icon-arrow-right')
export class IconArrowRight extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 26 26">
				<path
					stroke="currentColor"
					stroke-linecap="round"
					stroke-linejoin="round"
					d="m14.333 17 4-4m0 0-4-4m4 4H7.667M25 13c0 6.628-5.372 12-12 12-6.627 0-12-5.372-12-12C1 6.373 6.373 1 13 1c6.628 0 12 5.373 12 12Z"
				/>
			</svg>
		`;
	}
}

@customElement('icon-x')
export class IconX extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 26 26">
				<path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" d="m1 1 24 24m0-24L1 25" />
			</svg>
		`;
	}
}

@customElement('icon-check')
export class IconCheck extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 25">
				<path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" d="m.5 13.398 7.077 10.32L23.5.5" />
			</svg>
		`;
	}
}

@customElement('icon-email')
export class IconEmail extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24">
				<path
					fill="currentColor"
					fill-rule="evenodd"
					d="M12 13.812 9.513 11.75.836 20.633h22.107l-8.517-8.898L12 13.812Zm3.668-3.02 8.251 8.58A1.9 1.9 0 0 0 24 18.85V4.02l-8.332 6.772ZM0 3.988v14.864c0 .182.034.354.081.52l8.28-8.55L0 3.986Zm23.25-1.362H.75L12 11.639l11.25-9.014Z"
					clip-rule="evenodd"
				/>
			</svg>
		`;
	}
}

@customElement('icon-spinner')
export class IconSpinner extends BaseElement {
	override render() {
		return html`
			<svg viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg">
				<defs>
					<linearGradient x1="8.042%" y1="0%" x2="65.682%" y2="23.865%" id="a">
						<stop stop-color="#fff" stop-opacity="0" offset="0%" />
						<stop stop-color="#fff" stop-opacity=".631" offset="63.146%" />
						<stop stop-color="#fff" offset="100%" />
					</linearGradient>
				</defs>
				<g fill="none" fill-rule="evenodd">
					<g transform="translate(1 1)">
						<path d="M36 18c0-9.94-8.06-18-18-18" id="Oval-2" stroke="url(#a)" stroke-width="2">
							<animateTransform
								attributeName="transform"
								type="rotate"
								from="0 18 18"
								to="360 18 18"
								dur="0.9s"
								repeatCount="indefinite"
							/>
						</path>
						<circle fill="#fff" cx="36" cy="18" r="1">
							<animateTransform
								attributeName="transform"
								type="rotate"
								from="0 18 18"
								to="360 18 18"
								dur="0.9s"
								repeatCount="indefinite"
							/>
						</circle>
					</g>
				</g>
			</svg>
		`;
	}
}

@customElement('icon-chevron-down')
export class IconChevronDown extends BaseElement {
	override render() {
		return html`
			<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
				<g clip-path="url(#a)">
					<path
						stroke="currentColor"
						stroke-linecap="round"
						stroke-linejoin="round"
						stroke-width="4"
						d="m0 6 12 12L24 6"
					/>
				</g>
				<defs>
					<clipPath id="a">
						<path fill="currentColor" d="M0 0h24v24H0z" />
					</clipPath>
				</defs>
			</svg>
		`;
	}
}
